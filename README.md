# knowledge

In this project, you will learn about implementing Microservice Architecture in Java stuck.
We will be implementing different libraries and techniques to implement a complete architecture.
 
1-Creating Customer Service
</br>
2-Creating a Order Service
</br>
3-Implementing Service Registry
</br>
4-Implementing API Gateway with load balancer
</br>
5-Creating Hystrix Dashboard 
</br>
6-Cloud Config Server
</br>
7-GitHub Repo for Config Server
</br>
8-Implement Zipkin and Sleuth
</br>
We use https://start.spring.io/ to generate templeate project.


